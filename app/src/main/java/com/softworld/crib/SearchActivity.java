package com.softworld.crib;

import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softworld.crib.adapters.ItemsAdapter;
import com.softworld.crib.dependencies.VolleyErrors;
import com.softworld.crib.models.Crib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SearchActivity extends AppCompatActivity {

    private SweetAlertDialog pDialog, wDialog, wDialog2;
    private RecyclerView recyclerView;
    private List<Crib> cribList;
    private String  location;
    private String searchCribsUrl = "http://crib.co.ke/app/index.php/api/search_kribs";


    public static final String KEY_LOCATION = "location";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.searchToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(R.string.searchToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        Intent intent = getIntent();

        recyclerView = (RecyclerView) findViewById(R.id.search_recycler_view);

        pDialog = new SweetAlertDialog(SearchActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Searching...");

        wDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        wDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        wDialog.setTitleText("Error");

        cribList = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            location = intent.getStringExtra(SearchManager.QUERY);
            processSearch(location);
        } else {
            wDialog2 = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
            wDialog2.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            wDialog2.setContentText("Please download an update of Krib from playstore to use this feature");
            wDialog2.setTitleText("Update");
            wDialog2.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    startActivity(new Intent(SearchActivity.this, MainActivity.class));
                    finish();
                }
            });
            wDialog2.show();
        }
    }


    private void processSearch(final String location) {
        pDialog.show();
        StringRequest req = new StringRequest(Request.Method.POST, searchCribsUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONArray cribssArray = new JSONArray(response);

                            if (cribssArray.length() >0)
                            {
                                for (int i = 0; i < cribssArray.length(); i++) {

                                    JSONObject cribsObject = (JSONObject) cribssArray.get(i);

                                    int propertyId, bedrooms,bathrooms;
                                    String tagline,price,description,
                                            features,contactPhone,contactMail,latLong, thumbnail, configs,deposit,
                                            water, availability, electricity;


                                    try {
                                        configs = cribsObject.getString("configs");

                                    } catch (JSONException je) {
                                        configs = "N/A";

                                    }

                                    try {
                                        deposit = cribsObject.getString("deposits");

                                    } catch (JSONException je) {
                                        deposit = "N/A";

                                    }

                                    try {
                                        water = cribsObject.getString("water");

                                    } catch (JSONException je) {
                                        water = "N/A";

                                    }

                                    try {
                                        availability = cribsObject.getString("availability");

                                    } catch (JSONException je) {
                                        availability = "N/A";

                                    }

                                    try {
                                        electricity = cribsObject.getString("electricity");

                                    } catch (JSONException je) {
                                        electricity = "N/A";

                                    }
                                    try {
                                        propertyId = Integer.parseInt(cribsObject.getString("id"));

                                    } catch (JSONException je) {
                                        propertyId = 0;

                                    }
                                    try {
                                        tagline = cribsObject.getString("tagline");

                                    } catch (JSONException je) {
                                        tagline = "N/A";

                                    }

                                    try {
                                        price = cribsObject.getString("price");

                                    } catch (JSONException je) {
                                        price = "N/A";

                                    }
                                    try {
                                        description = cribsObject.getString("description");

                                    } catch (JSONException je) {
                                        description = "N/A";

                                    }
                                    try {
                                        bedrooms = Integer.parseInt(cribsObject.getString("bedrooms"));

                                    } catch (JSONException je) {
                                        bedrooms = 0;

                                    }
                                    try {
                                        bathrooms = Integer.parseInt(cribsObject.getString("bathrooms"));

                                    } catch (JSONException je) {
                                        bathrooms = 0;
                                    }
                                    try {
                                        features = cribsObject.getString("features");

                                    } catch (JSONException je) {
                                        features = "N/A";

                                    }
                                    try {
                                        contactPhone = cribsObject.getString("contact_phone");

                                    } catch (JSONException je) {
                                        contactPhone = "N/A";

                                    }
                                    try {
                                        contactMail = cribsObject.getString("contact_mail");

                                    } catch (JSONException je) {
                                        contactMail = "N/A";

                                    }
                                    try {
                                        latLong = cribsObject.getString("lat_long");

                                    } catch (JSONException je) {
                                        latLong = "N/A";

                                    }
                                    try {
                                        thumbnail = cribsObject.getString("thumbnail");

                                    } catch (JSONException je) {
                                        thumbnail = "N/A";

                                    }

                                    Crib newCrib = new Crib(propertyId,bedrooms,bathrooms,tagline,price,description,features,contactPhone,contactMail,latLong,thumbnail,configs,deposit,water,availability,electricity);
                                    cribList.add(newCrib);

                                }
                                ItemsAdapter adapter = new ItemsAdapter(SearchActivity.this, cribList);
                                recyclerView.setAdapter(adapter);
                                pDialog.dismiss();
                            }
                            else
                            {
                                pDialog.dismiss();
                                wDialog.setTitleText("No results");
                                wDialog.setContentText("No results were found with your search.");
                                wDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        wDialog.dismiss();
                                        startActivity(new Intent(SearchActivity.this, MainActivity.class));
                                        finish();
                                    }
                                });
                                wDialog.show();
                            }


                        } catch (JSONException e) {
                            pDialog.dismiss();
                            new SweetAlertDialog(SearchActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Error")
                                    .setContentText(e.getMessage())
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            startActivity(new Intent(SearchActivity.this, MainActivity.class));
                                            finish();
                                        }

                                    })
                                    .show();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                errorMessage.setVisibility(View.VISIBLE);
                pDialog.dismiss();
                wDialog.setContentText(VolleyErrors.getVolleyErrorMessages(error, SearchActivity.this));
                wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        wDialog.dismiss();
                        startActivity(new Intent(SearchActivity.this, MainActivity.class));
                        finish();
                    }
                });
                wDialog.show();
            }

        }){
            @Override
            protected Map<String, String> getParams()
            {
                Map <String, String> params = new HashMap<String, String>();
                params.put(KEY_LOCATION, location);
                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(SearchActivity.this);
        requestQueue.add(req);
    }

}
