package com.softworld.crib;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.softworld.crib.adapters.CustomPageAdapter;
import com.softworld.crib.dependencies.ImageBase64;
import com.softworld.crib.dependencies.MyCommand;
import com.softworld.crib.dependencies.NumberTextWatcher;
import com.softworld.crib.dependencies.UserLocalStore;
import com.softworld.crib.dependencies.VolleyErrors;
import com.softworld.crib.models.DataObject;
import com.softworld.crib.models.PostCrib;
import com.softworld.crib.models.User;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.iwf.photopicker.PhotoPicker;
import me.iwf.photopicker.PhotoPreview;

public class PostCribActivity extends AppCompatActivity {

    private Toolbar postToolbar;
    private Spinner configSpinner,electricitySpinner,bedroomsSpinner,bathroomsSpinner, waterSpinner;
    private EditText deposit, price,tagline, description,
            availability, phoneNo;

    private FloatingActionButton addPhotos;
    int PLACE_PICKER_REQUEST = 1;
    private TextView locationAddress;
    private ArrayList<String> selectedPhotos = new ArrayList<>();
    private ViewPager viewPager;
    private CustomPageAdapter mCustomPagerAdapter;
    private RadioButton leaseTypeButton;
    private RadioGroup leaseTypeGroup;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private Button continuePosting;
    private String url = "http://crib.co.ke/app/index.php/api/post_new_crib";
    private SweetAlertDialog pDialog, successDialog, wDialog;
    private Bitmap displayImageBitmap;
    private UserLocalStore userLocalStore;
    private Dialog addPhotosDialog, payDialog;
    private User loggedInUser;



    MyCommand myCommand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_crib);

        postToolbar = (Toolbar) findViewById(R.id.postToolbar);
        setSupportActionBar(postToolbar);

        getSupportActionBar().setTitle(R.string.postKrib);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myCommand = new MyCommand(getApplicationContext());
        userLocalStore = new UserLocalStore(getApplicationContext());

        loggedInUser = userLocalStore.getLoggedInUser();


        locationAddress = (TextView)findViewById(R.id.locationAddress);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

        configSpinner = (Spinner)findViewById(R.id.configSpinner);
        electricitySpinner = (Spinner)findViewById(R.id.electricitySpinner);
        bedroomsSpinner = (Spinner)findViewById(R.id.bedroomsSpinner);
        bathroomsSpinner = (Spinner)findViewById(R.id.bathroomsSpinner);
        waterSpinner = (Spinner)findViewById(R.id.waterSpinner);
        leaseTypeGroup = (RadioGroup)findViewById(R.id.leaseTypeRadioGroup);

        ArrayAdapter<CharSequence> electricityAdapter = ArrayAdapter.createFromResource(this, R.array.electricity, android.R.layout.simple_spinner_item);
        electricityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        electricitySpinner.setAdapter(electricityAdapter);

        ArrayAdapter<CharSequence> configAdapter = ArrayAdapter.createFromResource(this, R.array.configs, android.R.layout.simple_spinner_item);
        configAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        configSpinner.setAdapter(configAdapter);

        ArrayAdapter<CharSequence> bathroomsAdapter = ArrayAdapter.createFromResource(this, R.array.reg_bathroom, android.R.layout.simple_spinner_item);
        bathroomsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bathroomsSpinner.setAdapter(bathroomsAdapter);


        ArrayAdapter<CharSequence> waterAdapter = ArrayAdapter.createFromResource(this, R.array.reg_water, android.R.layout.simple_spinner_item);
        bathroomsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        waterSpinner.setAdapter(waterAdapter);

        ArrayAdapter<CharSequence> bedroomsAdapter = ArrayAdapter.createFromResource(this, R.array.reg_bedroom, android.R.layout.simple_spinner_item);
        bedroomsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bedroomsSpinner.setAdapter(bedroomsAdapter);

        deposit = (EditText)findViewById(R.id.deposit);
        deposit.addTextChangedListener(new NumberTextWatcher(deposit, "#,###"));

        price = (EditText)findViewById(R.id.price);
        price.addTextChangedListener(new NumberTextWatcher(price, "#,###"));

        tagline = (EditText)findViewById(R.id.tagline);
        description = (EditText)findViewById(R.id.description);
        availability = (EditText)findViewById(R.id.availability);
        phoneNo = (EditText)findViewById(R.id.phoneNo);

        addPhotos = (FloatingActionButton)findViewById(R.id.photosFAB);
        continuePosting = (Button)findViewById(R.id.continuePosting);

        viewPager = (ViewPager) findViewById(R.id.uploadViewpager);
        viewPager.setVisibility(View.GONE);

        availability.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    setDateTimeField();
                }
            }
        });

        addPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoPicker.builder()
                        .setPhotoCount(1)
                        .setShowCamera(true)
                        .setShowGif(true)
                        .setPreviewEnabled(false)
                        .start(PostCribActivity.this, PhotoPicker.REQUEST_CODE);

            }
        });

        continuePosting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String priceTxt = "";
                String depositTxt = "";
                int selected = leaseTypeGroup.getCheckedRadioButtonId();
                leaseTypeButton = (RadioButton)findViewById(selected);
                String selectedLeaseType = leaseTypeButton.getText().toString();
                String leaseTypeTxt;
                if (selectedLeaseType.equals(R.string.to_let))
                {
                    leaseTypeTxt = "1";
                }
                else
                {
                    leaseTypeTxt = "2";
                }

                String taglineTxt = tagline.getText().toString();
                String descriptionTxt = description.getText().toString();
                String availabilityTxt = availability.getText().toString();
                String waterTxt = waterSpinner.getSelectedItem().toString();
                String phoneNoTxt = phoneNo.getText().toString();
                String emailTxt = userLocalStore.getLoggedInUser().getPersonEmail();
                String configsTxt = configSpinner.getSelectedItem().toString();
                if (!TextUtils.isEmpty(price.getText().toString()))
                {
                    String priceArr[] = price.getText().toString().split("\\.");
                    priceTxt = priceArr[1].replace(",","");
                }

                if (!TextUtils.isEmpty(deposit.getText().toString()))
                {
                    String depoArr[] = deposit.getText().toString().split("\\.");
                    depositTxt = depoArr[1].replace(",","");
                }


                String bedroomsArr[] = bedroomsSpinner.getSelectedItem().toString().split(" ");
                String bedroomsTxt = bedroomsArr[0];
                String bathroomsArr[] = bathroomsSpinner.getSelectedItem().toString().split(" ");
                String bathroomsTxt = bathroomsArr[0];
                String electricityTxt = electricitySpinner.getSelectedItem().toString();

                PostCrib postCrib = new PostCrib(selectedPhotos,leaseTypeTxt,taglineTxt,descriptionTxt,configsTxt,
                        bedroomsTxt,bathroomsTxt,priceTxt,depositTxt,availabilityTxt,electricityTxt,
                        waterTxt,phoneNoTxt,emailTxt);

                if (validateKrib(postCrib))
                {
                    try {
                        displayImageBitmap = com.softworld.crib.dependencies.ImageLoader.init()
                                .from(postCrib.getSelectedPhotos().get(0))
                                .requestSize(720, 720).getBitmap();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    startUploadKrib(postCrib);
                    pDialog = new SweetAlertDialog(PostCribActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog.setTitleText("Processing...");
                    pDialog.setCancelable(false);
                    pDialog.show();
//

                }
            }
        });

        wDialog = new SweetAlertDialog(PostCribActivity.this, SweetAlertDialog.WARNING_TYPE);
        wDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        wDialog.setTitleText("Error :(");

        successDialog = new SweetAlertDialog(PostCribActivity.this, SweetAlertDialog.SUCCESS_TYPE);
        successDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        successDialog.setTitleText("Success! :)");

    }




    private boolean validateKrib(PostCrib postCrib) {
        boolean valid = true;

        if(postCrib.getSelectedPhotos().size() == 0)
        {
            Toast.makeText(PostCribActivity.this, R.string.select_photo, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }

        if(TextUtils.isEmpty(postCrib.getTaglineTxt()))
        {
            Toast.makeText(PostCribActivity.this, R.string.set_tagline, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }

        if(TextUtils.isEmpty(postCrib.getDescriptionTxt()))
        {
            Toast.makeText(PostCribActivity.this, R.string.set_description, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }

        if(TextUtils.isEmpty(postCrib.getPriceTxt()))
        {
            Toast.makeText(PostCribActivity.this, R.string.set_price, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }

        if(TextUtils.isEmpty(postCrib.getDepositTxt()))
        {
            Toast.makeText(PostCribActivity.this, R.string.set_depo, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }

        if(TextUtils.isEmpty(postCrib.getAvailabilityTxt()))
        {
            Toast.makeText(PostCribActivity.this, R.string.set_date, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }


        if(TextUtils.isEmpty(postCrib.getPhoneNoTxt()))
        {
            Toast.makeText(PostCribActivity.this, R.string.set_phone_no, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }
        return valid;
    }

    private void setDateTimeField() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                availability.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK &&
                (requestCode == PhotoPicker.REQUEST_CODE || requestCode == PhotoPreview.REQUEST_CODE)) {

            List<String> photos = null;
            if (data != null) {
                photos = data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);
            }
            selectedPhotos.clear();

            if (photos != null) {

                selectedPhotos.addAll(photos);

                viewPager.setVisibility(View.VISIBLE);
                List<DataObject> imagesData = new ArrayList<DataObject>();

                for (int j = 0; j < selectedPhotos.size(); j++) {
                    imagesData.add(new DataObject(selectedPhotos.get(j)));
                }
                mCustomPagerAdapter = new CustomPageAdapter(PostCribActivity.this, imagesData);
                viewPager.setAdapter(mCustomPagerAdapter);
                Timer timer = new Timer();
                timer.schedule(new PostCribActivity.UpdateTimeTask(), 1000, 3000);
            }
        }

    }

    class UpdateTimeTask extends TimerTask {
        public void run() {
            viewPager.post(new Runnable() {
                public void run() {

                    if (viewPager.getCurrentItem() < mCustomPagerAdapter
                            .getCount() - 1) {
                        viewPager.setCurrentItem(
                                viewPager.getCurrentItem() + 1, true);
                    } else {
                        viewPager.setCurrentItem(0, true);

                    }
                }
            });
        }
    }

    private void startUploadKrib(final PostCrib postCrib) {

        final String encodedDisplayImage = ImageBase64.encode(displayImageBitmap);

        StringRequest infoStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                pDialog.dismiss();
                successDialog.setContentText(getString(R.string.krib_posted));
                successDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        successDialog.dismiss();
                        showAddPhotosDialog(response);
                    }
                });
                successDialog.show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //error.printStackTrace();
                pDialog.dismiss();
                wDialog.setContentText(VolleyErrors.getVolleyErrorMessages(error, PostCribActivity.this));
                wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        wDialog.dismiss();
                        startActivity(new Intent(PostCribActivity.this, MainActivity.class));
                        finish();
                    }
                });
                wDialog.show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("availability", postCrib.getAvailabilityTxt());
                params.put("bathrooms", postCrib.getBathroomsTxt());
                params.put("bedrooms", postCrib.getBedroomsTxt());
                params.put("configs", postCrib.getConfigsTxt());
                params.put("deposit", postCrib.getDepositTxt());
                params.put("description", postCrib.getDescriptionTxt());
                params.put("electricity", postCrib.getElectricityTxt());
                params.put("email", postCrib.getEmailTxt());
                params.put("leaseType", postCrib.getLeaseTypeTxt());
                params.put("phoneNo", postCrib.getPhoneNoTxt());
                params.put("price", postCrib.getPriceTxt());
                params.put("tagline", postCrib.getTaglineTxt());
                params.put("water", postCrib.getWaterTxt());
                params.put("displayImage", encodedDisplayImage);

                return params;
            }
        };

        postCrib.getSelectedPhotos().clear();

        infoStringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(PostCribActivity.this);
        requestQueue.add(infoStringRequest);

    }


    private void showAddPhotosDialog(final String response) {
        addPhotosDialog = new Dialog(PostCribActivity.this);
        addPhotosDialog.setContentView(R.layout.add_photos_dialog);


        Button addNow, addLater;

        addLater = (Button) addPhotosDialog.findViewById(R.id.addLater);
        addLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPhotosDialog.dismiss();
                //showPayDialog();
                startActivity(new Intent(PostCribActivity.this, MainActivity.class));
                finish();
            }
        });



        addNow = (Button) addPhotosDialog.findViewById(R.id.addNow);
        addNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent mIntent = new Intent(PostCribActivity.this, MyProperty.class);
                mIntent.putExtra("myEmail", loggedInUser.getPersonEmail());
                startActivity(mIntent);
                finish();
            }
        });
        addPhotosDialog.show();
    }

    private void showPayDialog() {
        payDialog = new Dialog(PostCribActivity.this);
        payDialog.setContentView(R.layout.pay_dialog);


        Button payNow, payLater;

        payLater = (Button) payDialog.findViewById(R.id.payLater);
        payLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payDialog.dismiss();

                final SweetAlertDialog sweetAlert = new SweetAlertDialog(PostCribActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                sweetAlert.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                sweetAlert.setTitleText("Noted! :)");
                sweetAlert.setContentText(getString(R.string.pay_later));
                sweetAlert.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlert.dismiss();
                        startActivity(new Intent(PostCribActivity.this, MainActivity.class));
                        finish();
                    }
                });
                sweetAlert.show();


            }
        });



        payNow = (Button) payDialog.findViewById(R.id.payNow);
        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payDialog.dismiss();
                //initCheckout();
                startActivity(new Intent(PostCribActivity.this, MainActivity.class));
                finish();
            }
        });
        payDialog.show();
    }
}
