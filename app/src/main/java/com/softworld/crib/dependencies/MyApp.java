package com.softworld.crib.dependencies;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import com.orm.SugarContext;

/**
 * Created by muoki on 11/30/16.
 */

public class MyApp extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }
}
