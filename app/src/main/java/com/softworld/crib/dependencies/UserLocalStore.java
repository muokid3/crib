package com.softworld.crib.dependencies;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.softworld.crib.models.User;


/**
 * Created by muoki on 2/1/2016.
 */
public class UserLocalStore {
    public static String SP_NAME = "userDetails";
    SharedPreferences userLocalDatabase;

    public UserLocalStore(Context context)
    {
        userLocalDatabase = context.getSharedPreferences(SP_NAME,0);
    }

    public void storeUserData(User user)
    {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("personName", user.getPersonName());
        spEditor.putString("personEmail", user.getPersonEmail());
        spEditor.putString("personPhoto", user.getPersonPhoto().toString());
        spEditor.commit();
    }

    public User getLoggedInUser()
    {
        String personName = userLocalDatabase.getString("personName","");
        String personEmail = userLocalDatabase.getString("personEmail","");
        String personPhoto = userLocalDatabase.getString("personPhoto","");

        Uri photoUri = Uri.parse(personPhoto);

        User newUser = new User(personName,personEmail,photoUri);
        return newUser;
    }

    public void setUserLoggedIn(boolean loggedIn)
    {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putBoolean("loggedIn", loggedIn);
        spEditor.commit();
    }

    public boolean getBoolUserLoggedIn()
    {
        if (userLocalDatabase.getBoolean("loggedIn", false) == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void clearUserData()
    {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.clear();
        spEditor.commit();
    }
}
