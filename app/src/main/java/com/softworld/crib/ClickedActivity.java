package com.softworld.crib;

import android.*;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.softworld.crib.adapters.CustomPageAdapter;
import com.softworld.crib.models.Crib;
import com.softworld.crib.models.DataObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ClickedActivity extends AppCompatActivity {

    Toolbar clickedToolbar;
    Crib clickedCrib;
//    private ImageView mapImage;
    private String apiKey = "AIzaSyAHhSUFQpM--L0BD0RNmnf_-JiDY6MhsuU";//+R.string.my_google_map_api_key;
    private ImageView houseThumbnail;
    private TextView tagline, location, priceText, signedCopy, bathrooms, bedrooms,
            smallDesc, config, water, electricity,deposit, availability;
    private ViewPager viewPager;
    private BottomSheetBehavior bottomSheetBehavior;
    private TextView bottomSheetHeading;
    private Button contact, callNow, requestBtn;
    private String cribImagesUrl = "http://crib.co.ke/app/index.php/api/get_krib_images";
    private List<String> imageSourceArray;
    private CustomPageAdapter mCustomPagerAdapter;
    private EditText reqCallbackNumber;
    private SweetAlertDialog successDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clicked);

        clickedToolbar = (Toolbar) findViewById(R.id.clickedToolbar);
        setSupportActionBar(clickedToolbar);

        Intent i = getIntent();
        clickedCrib = (Crib) i.getSerializableExtra("clickedObject");

        getSupportActionBar().setTitle(clickedCrib.getTagline());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



//        mapImage = (ImageView) findViewById(R.id.mapImage);
        houseThumbnail = (ImageView) findViewById(R.id.houseThumbnail);
        tagline = (TextView) findViewById(R.id.tagline);
        location = (TextView) findViewById(R.id.location);
        priceText = (TextView) findViewById(R.id.priceText);
        signedCopy = (TextView) findViewById(R.id.signedCopy);
        bathrooms = (TextView) findViewById(R.id.bathrooms);
        bedrooms = (TextView) findViewById(R.id.bedrooms);
        smallDesc = (TextView) findViewById(R.id.smallDesc);
        config = (TextView) findViewById(R.id.config);
        water = (TextView) findViewById(R.id.water);
        electricity = (TextView) findViewById(R.id.electricity);
        deposit = (TextView) findViewById(R.id.deposit);
        availability = (TextView) findViewById(R.id.availability);
        contact = (Button) findViewById(R.id.contact);
        callNow = (Button) findViewById(R.id.callNow);
        requestBtn = (Button) findViewById(R.id.requestBtn);
        reqCallbackNumber = (EditText)findViewById(R.id.reqCallbackNumber);

        tagline.setText(clickedCrib.getTagline());
        String formatedPriceText = NumberFormat.getNumberInstance(Locale.US)
                .format(Double.parseDouble(clickedCrib.getPrice()));
        priceText.setText(formatedPriceText);
        signedCopy.setText(R.string.price);
        bedrooms.setText(clickedCrib.getBedrooms()+"");
        bathrooms.setText(clickedCrib.getBathrooms()+"");
        smallDesc.setText(clickedCrib.getDescription());
        config.setText(clickedCrib.getConfigs());
        water.setText(clickedCrib.getWater());
        electricity.setText(clickedCrib.getElectricity());
        String formatedDeposit = NumberFormat.getNumberInstance(Locale.US)
                .format(Double.parseDouble(clickedCrib.getDeposit()));
        deposit.setText(formatedDeposit);
        availability.setText(clickedCrib.getAvailability());


//        String url = "http://maps.google.com/maps/api/staticmap?center="
//                + clickedCrib.getLatLong()
//                + "&markers=color:red%7Clabel:C%7C" + clickedCrib.getLatLong() + "&"
//                + "&zoom=15&size=600x300&key=" + apiKey;
//
//        Glide.with(this).load(url)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(mapImage);

        Glide.with(this).load(clickedCrib.getThumbnail())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(houseThumbnail);

//        mapImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Uri uri = Uri.parse("geo:0,0?q="+clickedCrib.getLatLong()+" ("+clickedCrib.getLocation()+")");
//                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                startActivity(intent);
//            }
//        });


        loadImages();
        initViews();
        initListeners();

    }

    private void loadImages() {

        imageSourceArray = new ArrayList<>();
        StringRequest req = new StringRequest(Request.Method.POST, cribImagesUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONArray cribssArray = new JSONArray(response);

                            for (int i = 0; i < cribssArray.length(); i++) {
                                imageSourceArray.add(cribssArray.getString(i));
                            }

                            List<DataObject> data = new ArrayList<DataObject>();

                            for (int j = 0; j < imageSourceArray.size(); j++) {
                                data.add(new DataObject(imageSourceArray.get(j)));
                            }

                            viewPager = (ViewPager) findViewById(R.id.viewpager);
                            mCustomPagerAdapter = new CustomPageAdapter(ClickedActivity.this, data);
                            viewPager.setAdapter(mCustomPagerAdapter);

                            Timer timer = new Timer();
                            timer.schedule(new UpdateTimeTask(), 1000, 3000);


                        } catch (JSONException e) {
                            Toast.makeText(ClickedActivity.this, "An error occurred when fetching images", Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ClickedActivity.this, "Unable to fetch images. Please check you connection", Toast.LENGTH_LONG).show();
            }

        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("krib_id", String.valueOf(clickedCrib.getPropertyId()));
                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(ClickedActivity.this);
        requestQueue.add(req);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                clickedCrib = (Crib) data.getSerializableExtra("clickedCrib");
            }
        }
    }


    /**
     * method to initialize the views
     */
    private void initViews() {


        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomSheetLayout));
        bottomSheetHeading = (TextView) findViewById(R.id.bottomSheetHeading);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

    }


    /**
     * method to initialize the listeners
     */
    private void initListeners() {

        callNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int permissionCheck = ContextCompat.checkSelfPermission(ClickedActivity.this, android.Manifest.permission.CALL_PHONE);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                            ClickedActivity.this,
                            new String[]{android.Manifest.permission.CALL_PHONE},
                            1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",  clickedCrib.getContactPhone(), null));
                    startActivity(intent);

                }

            }
        });

        requestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(reqCallbackNumber.getText().toString()))
                {
                    Toast.makeText(ClickedActivity.this, "Please provide your phone number", Toast.LENGTH_LONG).show();
                }
                else {
                    successDialog = new SweetAlertDialog(ClickedActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                    successDialog.setTitleText("Sent");
                    successDialog.setContentText(getString(R.string.callback_sent));
                    successDialog.setConfirmText("Ok");
                    successDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            successDialog.dismiss();
                        }
                    });
                    successDialog.show();
                }

            }
        });

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        // Capturing the callbacks for bottom sheet
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetHeading.setText(getString(R.string.text_collapse_me));
                } else {
                    bottomSheetHeading.setText(getString(R.string.text_expand_me));
                }

                // Check Logs to see how bottom sheets behaves
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        //Log.e("Bottom Sheet Behaviour", "STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        //Log.e("Bottom Sheet Behaviour", "STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        //Log.e("Bottom Sheet Behaviour", "STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        //Log.e("Bottom Sheet Behaviour", "STATE_HIDDEN");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        //Log.e("Bottom Sheet Behaviour", "STATE_SETTLING");
                        break;
                }
            }


            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });
    }

    class UpdateTimeTask extends TimerTask {
        public void run() {
            viewPager.post(new Runnable() {
                public void run() {

                    if (viewPager.getCurrentItem() < mCustomPagerAdapter
                            .getCount() - 1) {
                        viewPager.setCurrentItem(
                                viewPager.getCurrentItem() + 1, true);
                    } else {
                        viewPager.setCurrentItem(0, true);

                    }
                }
            });
        }
    }
}
