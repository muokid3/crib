package com.softworld.crib.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.softworld.crib.AddPhotosActivity;
import com.softworld.crib.R;
import com.softworld.crib.models.MyCrib;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by muoki on 10/24/16.
 */

public class MyCribsAdapter extends RecyclerView.Adapter<MyCribsAdapter.MyViewHolder> {

    private Context mContext;
    private List<MyCrib> cribList;
    private LayoutInflater inflater;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count, bedrooms, tagline, cardBathrooms, status;
        public ImageView thumbnail;
        public ImageButton addImagesButton, payButton;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.price);
            count = (TextView) view.findViewById(R.id.description);
            bedrooms = (TextView) view.findViewById(R.id.bedrooms);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            tagline = (TextView) view.findViewById(R.id.location);
            cardBathrooms = (TextView) view.findViewById(R.id.cardBathrooms);
            addImagesButton = (ImageButton) view.findViewById(R.id.addImagesButton);
            payButton = (ImageButton) view.findViewById(R.id.payButton);


            payButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Coming Soon", Toast.LENGTH_LONG).show();
                }
            });

            addImagesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();

                    Intent mIntent = new Intent(mContext, AddPhotosActivity.class);
                    mIntent.putExtra("krib_id", String.valueOf(cribList.get(pos).getPropertyId()));
                    mContext.startActivity(mIntent);
                }
            });

        }
    }


    public MyCribsAdapter(Context mContext, List<MyCrib> albumList) {
        this.mContext = mContext;
        this.cribList = albumList;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View itemView = inflater.inflate(R.layout.item_card, parent, false);
        View itemView =LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_property, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        MyCrib crib = cribList.get(position);
        String formatedPrice = NumberFormat.getNumberInstance(Locale.US)
                .format(Double.parseDouble(crib.getPrice()));
        holder.title.setText(formatedPrice);
        holder.count.setText(crib.getLocation());
        holder.bedrooms.setText(crib.getBedrooms()+"");
        holder.tagline.setText(crib.getTagline());
        holder.cardBathrooms.setText(crib.getBathrooms()+"");

        if (crib.getPaid() == 1)
        {
//            holder.status.setText(R.string.paid);
//            holder.status.setTextColor(Color.GREEN);
            holder.payButton.setVisibility(View.GONE);

        }
        else
        {
//            holder.status.setText(R.string.not_paid);
//            holder.status.setTextColor(Color.RED);
            holder.payButton.setVisibility(View.VISIBLE);
        }

        // loading album cover using Glide library
        Glide.with(mContext).load(crib.getThumbnail()).placeholder(R.drawable.default_thumbnail).into(holder.thumbnail);
    }


    @Override
    public int getItemCount() {
        return cribList.size();
    }
}
