package com.softworld.crib.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.softworld.crib.ClickedActivity;
import com.softworld.crib.R;
import com.softworld.crib.models.Crib;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by muoki on 10/24/16.
 */

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.MyViewHolder> {

    private Context mContext;
    private List<Crib> cribList;
    private LayoutInflater inflater;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count, bedrooms, tagline, cardBathrooms;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.price);
            count = (TextView) view.findViewById(R.id.description);
            bedrooms = (TextView) view.findViewById(R.id.bedrooms);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            tagline = (TextView) view.findViewById(R.id.location);
            cardBathrooms = (TextView) view.findViewById(R.id.cardBathrooms);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();

                    Crib clickedCrib = cribList.get(pos);

                    Intent i = new Intent(mContext, ClickedActivity.class);
                    i.putExtra("clickedObject", clickedCrib);
                    mContext.startActivity(i);
                }
            });

        }
    }


    public ItemsAdapter(Context mContext, List<Crib> albumList) {
        this.mContext = mContext;
        this.cribList = albumList;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View itemView = inflater.inflate(R.layout.item_card, parent, false);
        View itemView =LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Crib crib = cribList.get(position);
        String formatedPrice = NumberFormat.getNumberInstance(Locale.US)
                .format(Double.parseDouble(crib.getPrice()));
        holder.title.setText(formatedPrice);
        holder.bedrooms.setText(crib.getBedrooms()+"");
        holder.tagline.setText(crib.getTagline());
        holder.cardBathrooms.setText(crib.getBathrooms()+"");

        // loading album cover using Glide library
        Glide.with(mContext).load(crib.getThumbnail()).placeholder(R.drawable.default_thumbnail).into(holder.thumbnail);
    }


    @Override
    public int getItemCount() {
        return cribList.size();
    }
}
