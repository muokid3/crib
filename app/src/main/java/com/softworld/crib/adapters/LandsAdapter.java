package com.softworld.crib.adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.softworld.crib.AddPhotosActivity;
import com.softworld.crib.ClickedActivity;
import com.softworld.crib.LandDealsActivity;
import com.softworld.crib.R;
import com.softworld.crib.models.Land;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by muoki on 10/24/16.
 */

public class LandsAdapter extends RecyclerView.Adapter<LandsAdapter.MyViewHolder> {

    private Context mContext;
    private List<Land> cribList;
    private LayoutInflater inflater;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView location, price, description;
        public ImageView thumbnail;
        public Button contact_dealer;

        public MyViewHolder(View view) {
            super(view);
            location = (TextView) view.findViewById(R.id.location);
            price = (TextView) view.findViewById(R.id.price);
            description = (TextView) view.findViewById(R.id.description);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            contact_dealer = (Button) view.findViewById(R.id.contact_dealer);
        }
    }


    public LandsAdapter(Context mContext, List<Land> albumList) {
        this.mContext = mContext;
        this.cribList = albumList;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View itemView = inflater.inflate(R.layout.item_card, parent, false);
        View itemView =LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_land, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Land crib = cribList.get(position);
        String formatedPrice = NumberFormat.getNumberInstance(Locale.US)
                .format(Double.parseDouble(crib.getPrice()));
        holder.location.setText("Location: "+crib.getLocation() );
        holder.price.setText("Ksh. "+formatedPrice);
        holder.description.setText(crib.getDescription());

        holder.contact_dealer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cellPhone = crib.getContactPhone();

                int permissionCheck = ContextCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                            (Activity) mContext,
                            new String[]{Manifest.permission.CALL_PHONE},
                            1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", cellPhone, null));
                    mContext.startActivity(intent);

                }
            }
        });


        // loading album cover using Glide library
        Glide.with(mContext).load(crib.getThumbnail()).placeholder(R.drawable.default_thumbnail).into(holder.thumbnail);
    }


    @Override
    public int getItemCount() {
        return cribList.size();
    }
}
