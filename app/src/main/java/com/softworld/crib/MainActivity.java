package com.softworld.crib;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.softworld.crib.adapters.ItemsAdapter;
import com.softworld.crib.dependencies.CircleTransform;
import com.softworld.crib.dependencies.UserLocalStore;
import com.softworld.crib.dependencies.VolleyErrors;
import com.softworld.crib.models.Crib;
import com.softworld.crib.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private UserLocalStore userLocalStore;

    private static String urlProfileImg;

    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtWebsite;

    private User loggedInUser;

    private RecyclerView recyclerView;
    private ItemsAdapter adapter;
    private List<Crib> cribList;
    private List users;

    private ImageView backdrop;
    private String imgUrl = "http://crib.co.ke/api/bgimage.jpg";

    private String hottestCribsUrl = "http://crib.co.ke/app/index.php/api/get_latest_kribs";
    private Dialog dialog;
    private RadioButton leaseTypeButton;
    private RadioGroup leaseTypeGroup;
    private SwipeRefreshLayout swipeRefreshLayout;

    //private GifImageView loadingDroid;
    private TextView errorMessage;

    private ArrayList<String> listOfImageSources;

    private EditText minAmount, maxAmount, location, name, message, tittle;
    private Spinner bedroomSpinner;
    private SweetAlertDialog successDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        successDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        successDialog.setTitleText("Sent");
        successDialog.setContentText(getString(R.string.message_sent));
        successDialog.setConfirmText("Ok");
        successDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                successDialog.dismiss();
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        backdrop = (ImageView)findViewById(R.id.backdrop);
        Glide.with(this).load(imgUrl)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(backdrop);

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        txtWebsite = (TextView) navHeader.findViewById(R.id.website);
        imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);


        errorMessage = (TextView)findViewById(R.id.errorMessage);
        cribList = new ArrayList<>();
        cribList = Crib.listAll(Crib.class);

        if (cribList.isEmpty())
        {
            errorMessage.setText(R.string.no_cribs_found);
            errorMessage.setVisibility(View.VISIBLE);
        }
        adapter = new ItemsAdapter(this, cribList);
        adapter.notifyDataSetChanged();

        userLocalStore = new UserLocalStore(this);
        mAuth = FirebaseAuth.getInstance();

        loggedInUser = userLocalStore.getLoggedInUser();
        urlProfileImg = loggedInUser.getPersonPhoto().toString();


        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                if (firebaseAuth.getCurrentUser() == null)
                {
                    //he is signed out
                    startActivity(new Intent(MainActivity.this, GetStartedActivity.class));
                    finish();
                }

            }
        };



        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                Toast.makeText(MainActivity.this, "Refreshing now", Toast.LENGTH_LONG).show();
                prepareCribs();
            }
        });



        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        loadNavHeader();
        initCollapsingToolbar();
        prepareCribs();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthStateListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthStateListener != null) {
            mAuth.removeAuthStateListener(mAuthStateListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                //Toast.makeText(getApplicationContext(), "MY NIGGA WE MADE IT", Toast.LENGTH_LONG).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, SearchActivity.class)));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
//        if (id == R.id.action_search) {
//            showSearchDialog();
//        }

        return super.onOptionsItemSelected(item);
    }

    public String cleanify(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length()-1)=='+') {
            str = str.substring(0, str.length()-1);
        }
        return str;
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_post) {
            //startActivity(new Intent(MainActivity.this, PostCribActivity.class));

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("What property would you like to list?")
                    .setCancelable(false)
                    .setPositiveButton("Krib", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(MainActivity.this, PostCribActivity.class));
                        }
                    })
                    .setNegativeButton("Land", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(MainActivity.this, PostLandActivity.class));
                            //dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();


        }else if (id == R.id.nav_land_deals) {
            startActivity(new Intent(MainActivity.this, LandDealsActivity.class));
        }else if (id == R.id.nav_my_property) {
            Intent mIntent = new Intent(MainActivity.this, MyProperty.class);
            mIntent.putExtra("myEmail", loggedInUser.getPersonEmail());
            startActivity(mIntent);
        }else if (id == R.id.nav_contactus) {
            dialog = new Dialog(MainActivity.this);
            dialog.setContentView(R.layout.contact_us);
            Button closeButton, sendBtn;

            name = (EditText)dialog.findViewById(R.id.name);
            message = (EditText)dialog.findViewById(R.id.message);
            tittle = (EditText)dialog.findViewById(R.id.tittle);

            closeButton = (Button) dialog.findViewById(R.id.close);
            sendBtn = (Button) dialog.findViewById(R.id.sendBtn);

            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            sendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.isEmpty(name.getText().toString()))
                    {
                        Toast.makeText(MainActivity.this, R.string.whats_your_name, Toast.LENGTH_LONG).show();
                    }
                    else {
                        if (TextUtils.isEmpty(message.getText().toString()))
                        {
                            Toast.makeText(MainActivity.this, R.string.whats_your_question, Toast.LENGTH_LONG).show();
                        }
                        else {
                            sendMessage(name.getText().toString(), tittle.getText().toString(), message.getText().toString());
                        }
                    }
                }
            });

            dialog.show();


        } else if (id == R.id.nav_faq) {
            Toast.makeText(this, "Coming Soon...", Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = getString(R.string.download_app);
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.download_crib_now));
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));

        } else if (id == R.id.nav_logout) {

            mAuth.signOut();
            LoginManager.getInstance().logOut();

            //userlocalstore signout
            userLocalStore.clearUserData();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void sendMessage(String name, String tittle, String message) {
        successDialog.show();
        dialog.dismiss();
    }


    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void loadNavHeader() {
        // name, website
        txtName.setText(loggedInUser.getPersonName());
        txtWebsite.setText(loggedInUser.getPersonEmail());

        // loading header background image
        Glide.with(this).load(imgUrl)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgNavHeaderBg);

        // Loading profile image
        Glide.with(this).load(urlProfileImg)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile);
    }


    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void prepareCribs() {

        StringRequest req = new StringRequest(Request.Method.GET, hottestCribsUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        errorMessage.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        try {

                            JSONArray cribssArray = new JSONArray(response);

                            Crib.deleteAll(Crib.class);

                            for (int i = 0; i < cribssArray.length(); i++) {

                                JSONObject cribsObject = (JSONObject) cribssArray.get(i);

                                int propertyId, bedrooms,bathrooms;
                                String tagline,price,description,
                                        features,contactPhone,contactMail,latLong, thumbnail, configs,deposit,
                                        water, availability, electricity;


                                try {
                                    configs = cribsObject.getString("configs");

                                } catch (JSONException je) {
                                    configs = "N/A";

                                }

                                try {
                                    deposit = cribsObject.getString("deposits");

                                } catch (JSONException je) {
                                    deposit = "N/A";

                                }

                                try {
                                    water = cribsObject.getString("water");

                                } catch (JSONException je) {
                                    water = "N/A";

                                }

                                try {
                                    availability = cribsObject.getString("availability");

                                } catch (JSONException je) {
                                    availability = "N/A";

                                }

                                try {
                                    electricity = cribsObject.getString("electricity");

                                } catch (JSONException je) {
                                    electricity = "N/A";

                                }
                                try {
                                    propertyId = Integer.parseInt(cribsObject.getString("id"));

                                } catch (JSONException je) {
                                    propertyId = 0;

                                }
                                try {
                                    tagline = cribsObject.getString("tagline");

                                } catch (JSONException je) {
                                    tagline = "N/A";

                                }

                                try {
                                    price = cribsObject.getString("price");

                                } catch (JSONException je) {
                                    price = "N/A";

                                }
                                try {
                                    description = cribsObject.getString("description");

                                } catch (JSONException je) {
                                    description = "N/A";

                                }
                                try {
                                    bedrooms = Integer.parseInt(cribsObject.getString("bedrooms"));

                                } catch (JSONException je) {
                                    bedrooms = 0;

                                }
                                try {
                                    bathrooms = Integer.parseInt(cribsObject.getString("bathrooms"));

                                } catch (JSONException je) {
                                    bathrooms = 0;
                                }
                                try {
                                    features = cribsObject.getString("features");

                                } catch (JSONException je) {
                                    features = "N/A";

                                }
                                try {
                                    contactPhone = cribsObject.getString("contact_phone");

                                } catch (JSONException je) {
                                    contactPhone = "N/A";

                                }
                                try {
                                    contactMail = cribsObject.getString("contact_mail");

                                } catch (JSONException je) {
                                    contactMail = "N/A";

                                }
                                try {
                                    latLong = cribsObject.getString("lat_long");

                                } catch (JSONException je) {
                                    latLong = "N/A";

                                }
                                try {
                                    thumbnail = cribsObject.getString("thumbnail");

                                } catch (JSONException je) {
                                    thumbnail = "N/A";

                                }

                                Crib newCrib = new Crib(propertyId,bedrooms,bathrooms,tagline,price,description,features,contactPhone,contactMail,latLong,thumbnail,configs,deposit,water,availability,electricity);
                                newCrib.save();

                            }
                            cribList.clear();
                            cribList = Crib.listAll(Crib.class);


                            if (cribList.isEmpty())
                            {
                                errorMessage.setText(R.string.no_cribs_found);
                                errorMessage.setVisibility(View.VISIBLE);
                            }

                            adapter = new ItemsAdapter(MainActivity.this, cribList);
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            swipeRefreshLayout.setRefreshing(false);

                        } catch (JSONException e) {
                            new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Error")
                                    .setContentText(e.getMessage())
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }

                                    })
                                    .show();
                        }

                    }
                    }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                errorMessage.setVisibility(View.VISIBLE);
                errorMessage.setText(VolleyErrors.getVolleyErrorMessages(error, MainActivity.this)+" Pull to refresh");

            }

        });

        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(req);
    }
}
