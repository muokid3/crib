package com.softworld.crib;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softworld.crib.adapters.LandsAdapter;
import com.softworld.crib.dependencies.VolleyErrors;
import com.softworld.crib.models.Land;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LandDealsActivity extends AppCompatActivity {

    private SweetAlertDialog pDialog, wDialog;
    private RecyclerView recyclerView;
    private List<Land> cribList;
    private String landDealsUrl = "http://crib.co.ke/app/index.php/api/land_deals";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_land_deals);

        Toolbar toolbar = (Toolbar) findViewById(R.id.myPropertyToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(R.string.land_deals);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.my_property_recycler_view);

        pDialog = new SweetAlertDialog(LandDealsActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Fetching...");

        wDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        wDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        wDialog.setTitleText("Error");

        cribList = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        //pDialog.show();
        processSearch();
    }

    private void processSearch() {
        pDialog.show();
        StringRequest req = new StringRequest(Request.Method.POST, landDealsUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONArray cribssArray = new JSONArray(response);

                            if (cribssArray.length() >0)
                            {
                                for (int i = 0; i < cribssArray.length(); i++) {

                                    JSONObject cribsObject = (JSONObject) cribssArray.get(i);

                                    int propertyId;
                                    String size,location,price,description, contactPhone,contactMail,latLong, thumbnail,deposit;



                                    try {
                                        size = cribsObject.getString("size");

                                    } catch (JSONException je) {
                                        size = "N/A";

                                    }
                                    try {
                                        deposit = cribsObject.getString("deposits");

                                    } catch (JSONException je) {
                                        deposit = "N/A";

                                    }



                                    try {
                                        propertyId = Integer.parseInt(cribsObject.getString("id"));

                                    } catch (JSONException je) {
                                        propertyId = 0;

                                    }

                                    try {
                                        location = cribsObject.getString("location");

                                    } catch (JSONException je) {
                                        location = "N/A";

                                    }

                                    try {
                                        price = cribsObject.getString("price");

                                    } catch (JSONException je) {
                                        price = "N/A";

                                    }
                                    try {
                                        description = cribsObject.getString("description");

                                    } catch (JSONException je) {
                                        description = "N/A";

                                    }


                                    try {
                                        contactPhone = cribsObject.getString("contact_phone");

                                    } catch (JSONException je) {
                                        contactPhone = "N/A";

                                    }
                                    try {
                                        contactMail = cribsObject.getString("contact_mail");

                                    } catch (JSONException je) {
                                        contactMail = "N/A";

                                    }
                                    try {
                                        latLong = cribsObject.getString("lat_long");

                                    } catch (JSONException je) {
                                        latLong = "N/A";

                                    }
                                    try {
                                        thumbnail = cribsObject.getString("thumbnail");

                                    } catch (JSONException je) {
                                        thumbnail = "N/A";

                                    }

                                    Land newLand = new Land(propertyId, size,
                                            location,price,description,contactPhone,contactMail,
                                            latLong,thumbnail,deposit);
                                    cribList.add(newLand);

                                }
                                LandsAdapter adapter = new LandsAdapter(LandDealsActivity.this, cribList);
                                recyclerView.setAdapter(adapter);
                                pDialog.dismiss();
                            }
                            else
                            {
                                pDialog.dismiss();
                                wDialog.setTitleText("No Results");
                                wDialog.setContentText("There are no land deals posted yet");
                                wDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        wDialog.dismiss();
                                        startActivity(new Intent(LandDealsActivity.this, MainActivity.class));
                                        finish();
                                    }
                                });
                                wDialog.show();
                            }


                        } catch (JSONException e) {
                            pDialog.dismiss();
                            new SweetAlertDialog(LandDealsActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Error")
                                    .setContentText(e.getMessage())
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            startActivity(new Intent(LandDealsActivity.this, MainActivity.class));
                                            finish();
                                        }

                                    })
                                    .show();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                errorMessage.setVisibility(View.VISIBLE);
                pDialog.dismiss();
                wDialog.setContentText(VolleyErrors.getVolleyErrorMessages(error, LandDealsActivity.this));
                wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        wDialog.dismiss();
                        startActivity(new Intent(LandDealsActivity.this, MainActivity.class));
                        finish();
                    }
                });
                wDialog.show();
            }

        });

        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(LandDealsActivity.this);
        requestQueue.add(req);
    }

}
