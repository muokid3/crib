package com.softworld.crib.models;

import android.net.Uri;


/**
 * Created by muoki on 10/23/16.
 */

public class User {
    String personName,personEmail;
    Uri personPhoto;

    public User() {
    }

    public User(String personName, String personEmail, Uri personPhoto) {
        this.personName = personName;
        this.personEmail = personEmail;
        this.personPhoto = personPhoto;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonEmail() {
        return personEmail;
    }

    public void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    public Uri getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(Uri personPhoto) {
        this.personPhoto = personPhoto;
    }
}
