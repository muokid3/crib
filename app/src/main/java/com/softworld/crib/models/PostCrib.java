package com.softworld.crib.models;

import java.util.ArrayList;

/**
 * Created by muoki on 11/30/16.
 */

public class PostCrib {
    ArrayList<String> selectedPhotos;
    String leaseTypeTxt,taglineTxt,descriptionTxt,configsTxt,
            bedroomsTxt,bathroomsTxt,priceTxt,depositTxt,availabilityTxt,electricityTxt,
            waterTxt,phoneNoTxt,emailTxt;

    public PostCrib(ArrayList<String> selectedPhotos, String leaseTypeTxt, String taglineTxt, String descriptionTxt, String configsTxt, String bedroomsTxt, String bathroomsTxt, String priceTxt, String depositTxt, String availabilityTxt, String electricityTxt, String waterTxt, String phoneNoTxt, String emailTxt) {
        this.selectedPhotos = selectedPhotos;
        this.leaseTypeTxt = leaseTypeTxt;
        this.taglineTxt = taglineTxt;
        this.descriptionTxt = descriptionTxt;
        this.configsTxt = configsTxt;
        this.bedroomsTxt = bedroomsTxt;
        this.bathroomsTxt = bathroomsTxt;
        this.priceTxt = priceTxt;
        this.depositTxt = depositTxt;
        this.availabilityTxt = availabilityTxt;
        this.electricityTxt = electricityTxt;
        this.waterTxt = waterTxt;
        this.phoneNoTxt = phoneNoTxt;
        this.emailTxt = emailTxt;
    }

    public ArrayList<String> getSelectedPhotos() {
        return selectedPhotos;
    }

    public void setSelectedPhotos(ArrayList<String> selectedPhotos) {
        this.selectedPhotos = selectedPhotos;
    }


    public String getLeaseTypeTxt() {
        return leaseTypeTxt;
    }

    public void setLeaseTypeTxt(String leaseTypeTxt) {
        this.leaseTypeTxt = leaseTypeTxt;
    }

    public String getTaglineTxt() {
        return taglineTxt;
    }

    public void setTaglineTxt(String taglineTxt) {
        this.taglineTxt = taglineTxt;
    }

    public String getDescriptionTxt() {
        return descriptionTxt;
    }

    public void setDescriptionTxt(String descriptionTxt) {
        this.descriptionTxt = descriptionTxt;
    }

    public String getConfigsTxt() {
        return configsTxt;
    }

    public void setConfigsTxt(String configsTxt) {
        this.configsTxt = configsTxt;
    }

    public String getBedroomsTxt() {
        return bedroomsTxt;
    }

    public void setBedroomsTxt(String bedroomsTxt) {
        this.bedroomsTxt = bedroomsTxt;
    }

    public String getBathroomsTxt() {
        return bathroomsTxt;
    }

    public void setBathroomsTxt(String bathroomsTxt) {
        this.bathroomsTxt = bathroomsTxt;
    }

    public String getPriceTxt() {
        return priceTxt;
    }

    public void setPriceTxt(String priceTxt) {
        this.priceTxt = priceTxt;
    }

    public String getDepositTxt() {
        return depositTxt;
    }

    public void setDepositTxt(String depositTxt) {
        this.depositTxt = depositTxt;
    }

    public String getAvailabilityTxt() {
        return availabilityTxt;
    }

    public void setAvailabilityTxt(String availabilityTxt) {
        this.availabilityTxt = availabilityTxt;
    }

    public String getElectricityTxt() {
        return electricityTxt;
    }

    public void setElectricityTxt(String electricityTxt) {
        this.electricityTxt = electricityTxt;
    }

    public String getWaterTxt() {
        return waterTxt;
    }

    public void setWaterTxt(String waterTxt) {
        this.waterTxt = waterTxt;
    }

    public String getPhoneNoTxt() {
        return phoneNoTxt;
    }

    public void setPhoneNoTxt(String phoneNoTxt) {
        this.phoneNoTxt = phoneNoTxt;
    }

    public String getEmailTxt() {
        return emailTxt;
    }

    public void setEmailTxt(String emailTxt) {
        this.emailTxt = emailTxt;
    }
}
