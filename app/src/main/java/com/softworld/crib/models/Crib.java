package com.softworld.crib.models;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by muoki on 10/31/16.
 */

@SuppressWarnings("serial")
public class Crib extends SugarRecord implements Serializable{

    int propertyId, bedrooms,bathrooms;
    String tagline,price,description,
            features,contactPhone,contactMail,latLong, thumbnail, configs,deposit,
            water, availability, electricity;

    public Crib() {
    }

    public Crib(int id, int bedrooms, int bathrooms, String tagline, String price, String description, String features, String contactPhone, String contactMail, String latLong, String thumbnail, String configs, String deposit, String water, String availability, String electricity) {
        this.propertyId = id;
        this.bedrooms = bedrooms;
        this.bathrooms = bathrooms;
        this.tagline = tagline;
        this.price = price;
        this.description = description;
        this.features = features;
        this.contactPhone = contactPhone;
        this.contactMail = contactMail;
        this.latLong = latLong;
        this.thumbnail = thumbnail;
        this.configs = configs;
        this.deposit = deposit;
        this.water = water;
        this.availability = availability;
        this.electricity = electricity;
    }


    public int getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(int propertyId) {
        this.propertyId = propertyId;
    }

    public int getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(int bedrooms) {
        this.bedrooms = bedrooms;
    }

    public int getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(int bathrooms) {
        this.bathrooms = bathrooms;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactMail() {
        return contactMail;
    }

    public void setContactMail(String contactMail) {
        this.contactMail = contactMail;
    }

    public String getLatLong() {
        return latLong;
    }

    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getConfigs() {
        return configs;
    }

    public void setConfigs(String configs) {
        this.configs = configs;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getWater() {
        return water;
    }

    public void setWater(String water) {
        this.water = water;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getElectricity() {
        return electricity;
    }

    public void setElectricity(String electricity) {
        this.electricity = electricity;
    }
}
