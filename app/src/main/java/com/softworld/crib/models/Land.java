package com.softworld.crib.models;

/**
 * Created by muoki on 11/07/2017.
 */

public class Land {
    int propertyId;
    String size,location,price,description, contactPhone,contactMail,latLong, thumbnail,deposit;

    public Land(int propertyId, String size, String location, String price, String description, String contactPhone, String contactMail, String latLong, String thumbnail, String deposit) {
        this.propertyId = propertyId;
        this.size = size;
        this.location = location;
        this.price = price;
        this.description = description;
        this.contactPhone = contactPhone;
        this.contactMail = contactMail;
        this.latLong = latLong;
        this.thumbnail = thumbnail;
        this.deposit = deposit;
    }

    public int getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(int propertyId) {
        this.propertyId = propertyId;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactMail() {
        return contactMail;
    }

    public void setContactMail(String contactMail) {
        this.contactMail = contactMail;
    }

    public String getLatLong() {
        return latLong;
    }

    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }
}
