package com.softworld.crib.models;

import java.util.ArrayList;

/**
 * Created by muoki on 11/30/16.
 */

public class PostLand {
    ArrayList<String> selectedPhotos;
    String latLong;
    String locationTxt;
    String descriptionTxt;
    String priceTxt;
    String depositTxt;
    String sizeTxt;

    public String getSizeTxt() {
        return sizeTxt;
    }

    public void setSizeTxt(String sizeTxt) {
        this.sizeTxt = sizeTxt;
    }

    String phoneNoTxt;
    String emailTxt;

    public PostLand(ArrayList<String> selectedPhotos, String latLong, String locationTxt, String descriptionTxt,
                    String priceTxt, String depositTxt, String sizeTxt, String phoneNoTxt, String emailTxt) {
        this.selectedPhotos = selectedPhotos;
        this.latLong = latLong;
        this.locationTxt = locationTxt;
        this.descriptionTxt = descriptionTxt;
        this.priceTxt = priceTxt;
        this.depositTxt = depositTxt;
        this.sizeTxt = sizeTxt;
        this.phoneNoTxt = phoneNoTxt;
        this.emailTxt = emailTxt;
    }

    public ArrayList<String> getSelectedPhotos() {
        return selectedPhotos;
    }

    public void setSelectedPhotos(ArrayList<String> selectedPhotos) {
        this.selectedPhotos = selectedPhotos;
    }

    public String getLatLong() {
        return latLong;
    }

    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

    public String getLocationTxt() {
        return locationTxt;
    }

    public void setLocationTxt(String locationTxt) {
        this.locationTxt = locationTxt;
    }

    public String getDescriptionTxt() {
        return descriptionTxt;
    }

    public void setDescriptionTxt(String descriptionTxt) {
        this.descriptionTxt = descriptionTxt;
    }

    public String getPriceTxt() {
        return priceTxt;
    }

    public void setPriceTxt(String priceTxt) {
        this.priceTxt = priceTxt;
    }

    public String getDepositTxt() {
        return depositTxt;
    }

    public void setDepositTxt(String depositTxt) {
        this.depositTxt = depositTxt;
    }

    public String getPhoneNoTxt() {
        return phoneNoTxt;
    }

    public void setPhoneNoTxt(String phoneNoTxt) {
        this.phoneNoTxt = phoneNoTxt;
    }

    public String getEmailTxt() {
        return emailTxt;
    }

    public void setEmailTxt(String emailTxt) {
        this.emailTxt = emailTxt;
    }
}
