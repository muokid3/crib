package com.softworld.crib.models;

/**
 * Created by muoki on 11/3/16.
 */

public class DataObject {
    private String imageId;
    public DataObject(String imageId) {
        this.imageId = imageId;
    }
    public String getImageId() {
        return imageId;
    }
    public void setImageId(String imageId) {
        this.imageId = imageId;
    }
}