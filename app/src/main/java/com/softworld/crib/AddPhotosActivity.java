package com.softworld.crib;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softworld.crib.adapters.CustomPageAdapter;
import com.softworld.crib.dependencies.ImageBase64;
import com.softworld.crib.dependencies.VolleyErrors;
import com.softworld.crib.models.DataObject;

import org.json.JSONArray;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.iwf.photopicker.PhotoPicker;
import me.iwf.photopicker.PhotoPreview;

public class AddPhotosActivity extends AppCompatActivity {

    private  String krib_id;
    private FloatingActionButton addMorePhotosFAB;
    private ViewPager viewPager;
    private Button uploadPhotos;
    private ArrayList<String> selectedPhotos = new ArrayList<>();
    private ArrayList<String> encodedPhotos = new ArrayList<>();
    private CustomPageAdapter mCustomPagerAdapter;
    private Bitmap displayImageBitmap;
    private SweetAlertDialog pDialog,successDialog, wDialog;
    private String encodedDisplayImage;
    private String url = "http://crib.co.ke/app/index.php/api/upload_photos";
    private Dialog payDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photos);

        Toolbar toolbar = (Toolbar) findViewById(R.id.add_photos_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(R.string.add_photos);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        krib_id = intent.getStringExtra("krib_id");

        viewPager = (ViewPager) findViewById(R.id.uploadViewpager);
        addMorePhotosFAB = (FloatingActionButton)findViewById(R.id.addMorePhotosFAB);
        uploadPhotos = (Button)findViewById(R.id.uploadPhotos);

        wDialog = new SweetAlertDialog(AddPhotosActivity.this, SweetAlertDialog.WARNING_TYPE);
        wDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        wDialog.setTitleText("Error :(");

        successDialog = new SweetAlertDialog(AddPhotosActivity.this, SweetAlertDialog.SUCCESS_TYPE);
        successDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        successDialog.setTitleText("Success! :)");

        addMorePhotosFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoPicker.builder()
                        .setPhotoCount(9)
                        .setShowCamera(true)
                        .setShowGif(true)
                        .setPreviewEnabled(false)
                        .start(AddPhotosActivity.this, PhotoPicker.REQUEST_CODE);

            }
        });

        uploadPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPhotos.size() == 0)
                {
                    Toast.makeText(AddPhotosActivity.this, R.string.please_select_photos, Toast.LENGTH_LONG).show();
                }
                else
                {

                    for (int i = 0; i < selectedPhotos.size(); i++)
                    {
                        try {
                            displayImageBitmap = com.softworld.crib.dependencies.ImageLoader.init()
                                    .from(selectedPhotos.get(i))
                                    .requestSize(720, 720).getBitmap();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        encodedDisplayImage = ImageBase64.encode(displayImageBitmap);
                        encodedPhotos.add(encodedDisplayImage);
                    }


                    pDialog = new SweetAlertDialog(AddPhotosActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog.setTitleText("Uploading...");
                    pDialog.setCancelable(false);
                    pDialog.show();

                    startUploadPhotos(krib_id, encodedPhotos);


                }

            }
        });

    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK &&
                (requestCode == PhotoPicker.REQUEST_CODE || requestCode == PhotoPreview.REQUEST_CODE)) {

            List<String> photos = null;
            if (data != null) {
                photos = data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);
            }
            selectedPhotos.clear();

            if (photos != null) {

                selectedPhotos.addAll(photos);

                List<DataObject> imagesData = new ArrayList<DataObject>();

                for (int j = 0; j < selectedPhotos.size(); j++) {
                    imagesData.add(new DataObject(selectedPhotos.get(j)));
                }
                mCustomPagerAdapter = new CustomPageAdapter(AddPhotosActivity.this, imagesData);
                viewPager.setAdapter(mCustomPagerAdapter);
                Timer timer = new Timer();
                timer.schedule(new AddPhotosActivity.UpdateTimeTask(), 1000, 3000);
            }
        }

    }

    class UpdateTimeTask extends TimerTask {
        public void run() {
            viewPager.post(new Runnable() {
                public void run() {

                    if (viewPager.getCurrentItem() < mCustomPagerAdapter
                            .getCount() - 1) {
                        viewPager.setCurrentItem(
                                viewPager.getCurrentItem() + 1, true);
                    } else {
                        viewPager.setCurrentItem(0, true);

                    }
                }
            });
        }
    }

    private void startUploadPhotos(final String krib_id, final ArrayList<String> encodedPhotos)
    {
        StringRequest infoStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                pDialog.dismiss();
                successDialog.setContentText(response);
                //successDialog.setContentText(getString(R.string.photos_uploaded));
                successDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        successDialog.dismiss();
                        //showPayDialog();
                        Toast.makeText(AddPhotosActivity.this, response, Toast.LENGTH_LONG).show();
                        startActivity(new Intent(AddPhotosActivity.this, MainActivity.class));
                        finish();
                    }
                });
                successDialog.show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                wDialog.setContentText(VolleyErrors.getVolleyErrorMessages(error, AddPhotosActivity.this));
                wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        wDialog.dismiss();
                        startActivity(new Intent(AddPhotosActivity.this, MainActivity.class));
                        finish();
                    }
                });
                wDialog.show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                JSONArray jsArray = new JSONArray(encodedPhotos);

                params.put("krib_id", krib_id);
                params.put("photos", jsArray.toString());

                return params;
            }
        };

        selectedPhotos.clear();

        infoStringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(AddPhotosActivity.this);
        requestQueue.add(infoStringRequest);

    }

    private void showPayDialog() {
        payDialog = new Dialog(AddPhotosActivity.this);
        payDialog.setContentView(R.layout.pay_dialog);


        Button payNow, payLater;

        payLater = (Button) payDialog.findViewById(R.id.payLater);
        payLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payDialog.dismiss();

                final SweetAlertDialog sweetAlert = new SweetAlertDialog(AddPhotosActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                sweetAlert.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                sweetAlert.setTitleText("Noted! :)");
                sweetAlert.setContentText(getString(R.string.pay_later));
                sweetAlert.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlert.dismiss();
                        startActivity(new Intent(AddPhotosActivity.this, MainActivity.class));
                        finish();
                    }
                });
                sweetAlert.show();


            }
        });



        payNow = (Button) payDialog.findViewById(R.id.payNow);
        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payDialog.dismiss();
                //initCheckout();
                startActivity(new Intent(AddPhotosActivity.this, MainActivity.class));
                finish();
            }
        });
        payDialog.show();
    }
}
