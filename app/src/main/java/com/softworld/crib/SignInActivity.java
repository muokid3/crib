package com.softworld.crib;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.softworld.crib.dependencies.UserLocalStore;
import com.softworld.crib.models.User;

import java.net.MalformedURLException;
import java.net.URL;


import cn.pedant.SweetAlert.SweetAlertDialog;

public class SignInActivity extends AppCompatActivity {


    private Uri.Builder builder;
    private Uri photoUrl;


    private Toolbar signInToolbar;

    private EditText signInEmailET, signInPasswordET;
    private Button signInBtn, regButton;

    private FirebaseAuth mAuth;

    private FirebaseAuth.AuthStateListener mAuthListener;


    private SweetAlertDialog pDialog, wDialog;
    private UserLocalStore userLocalStore;
    private  TextView forgotPassword;
    private LoginButton facebookLoginButton;
    private CallbackManager mCallbackManager;

    private String defaultUserPic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_sign_in);

        defaultUserPic = "http://crib.co.ke/api/profilepic.png";

        signInToolbar = (Toolbar)findViewById(R.id.signInToolbar);
        setSupportActionBar(signInToolbar);
        mAuth = FirebaseAuth.getInstance();

        userLocalStore = new UserLocalStore(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sign In");

        signInEmailET = (EditText)findViewById(R.id.signInEmailET);
        signInPasswordET = (EditText)findViewById(R.id.signInPasswordET);
        signInBtn = (Button)findViewById(R.id.signInBtn);
        regButton = (Button)findViewById(R.id.regButton);
        forgotPassword = (TextView)findViewById(R.id.forgotPassword);

        pDialog = new SweetAlertDialog(SignInActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Processing...");

        wDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        wDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        wDialog.setTitle("Warning");

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignInActivity.this, ForgotPassword.class));
            }
        });


        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSignIn();
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    String name = user.getDisplayName();
                    String email = user.getEmail();

                    if (user.getPhotoUrl() != null && !user.getPhotoUrl().equals(Uri.EMPTY)) {
                        //photo url is available
                        photoUrl = user.getPhotoUrl();
                    } else {
                        //photourl is null or empty
                        photoUrl = Uri.parse(defaultUserPic);

                    }

                    User loggedInUser = new User(name, email, photoUrl);
                    userLocalStore.storeUserData(loggedInUser);

                    startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    finish();
                } else {
                    // User is signed out
                }
            }
        };

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignInActivity.this, RegisterActivity.class));
            }
        });

        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();
        facebookLoginButton = (LoginButton) findViewById(R.id.facebookLoginButton);
        facebookLoginButton.setReadPermissions("email", "public_profile");
        facebookLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                //Log.d(TAG, "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                //Log.d(TAG, "facebook:onError", error);
                // ...
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void startSignIn() {
        String email = signInEmailET.getText().toString();
        String password = signInPasswordET.getText().toString();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password))
        {
            Toast.makeText(SignInActivity.this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
        }
        else
        {
            pDialog.setTitleText("Logging in...");
            pDialog.show();
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    pDialog.dismiss();
                    if (!task.isSuccessful())
                    {
                        try {
                            throw task.getException();
                        } catch(FirebaseAuthWeakPasswordException e) {
                            wDialog.setTitleText("Please use a strong password");
                            wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    wDialog.dismiss();
                                    startActivity(new Intent(SignInActivity.this, GetStartedActivity.class));
                                    finish();
                                }
                            });
                            wDialog.show();
                        } catch(FirebaseAuthInvalidCredentialsException e) {
                            wDialog.setTitleText("Invalid username or password.");
                            wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    wDialog.dismiss();
                                    startActivity(new Intent(SignInActivity.this, GetStartedActivity.class));
                                    finish();
                                }
                            });
                            wDialog.show();
                        } catch(FirebaseAuthUserCollisionException e) {
                            wDialog.setTitleText("A user with this email already exists");
                            wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    wDialog.dismiss();
                                    startActivity(new Intent(SignInActivity.this, GetStartedActivity.class));
                                    finish();
                                }
                            });
                            wDialog.show();
                        } catch(Exception e) {
                            wDialog.setTitleText("Unable to sign in");
                            wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    wDialog.dismiss();
                                    startActivity(new Intent(SignInActivity.this, GetStartedActivity.class));
                                    finish();
                                }
                            });
                            wDialog.show();
                        }
                    }
                }
            });
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {
        //Log.d(TAG, "handleFacebookAccessToken:" + token);
        pDialog.show();

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        pDialog.dismiss();
                        if (!task.isSuccessful()) {
                            //Log.w(TAG, "signInWithCredential", task.getException());

                            Toast.makeText(SignInActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }
}
