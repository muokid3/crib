package com.softworld.crib;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.softworld.crib.adapters.CustomPageAdapter;
import com.softworld.crib.dependencies.ImageBase64;
import com.softworld.crib.dependencies.MyCommand;
import com.softworld.crib.dependencies.NumberTextWatcher;
import com.softworld.crib.dependencies.UserLocalStore;
import com.softworld.crib.dependencies.VolleyErrors;
import com.softworld.crib.models.DataObject;
import com.softworld.crib.models.PostCrib;
import com.softworld.crib.models.PostLand;
import com.softworld.crib.models.User;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.iwf.photopicker.PhotoPicker;
import me.iwf.photopicker.PhotoPreview;

public class PostLandActivity extends AppCompatActivity {
    private Toolbar postToolbar;
    private EditText deposit, price,tagline, description,
            size, phoneNo;

    private FloatingActionButton addPhotos, addLocation;
    int PLACE_PICKER_REQUEST = 1;
    private String latLong;
    private TextView locationAddress;
    private ArrayList<String> selectedPhotos = new ArrayList<>();
    private ViewPager viewPager;
    private CustomPageAdapter mCustomPagerAdapter;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private Button continuePosting;
    private String url = "http://crib.co.ke/app/index.php/api/post_new_land";
    private SweetAlertDialog pDialog, successDialog, wDialog;
    private Bitmap displayImageBitmap;
    private UserLocalStore userLocalStore;
    private String location;
    private Dialog addPhotosDialog, payDialog;
    private User loggedInUser;

    MyCommand myCommand;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_land);

        postToolbar = (Toolbar) findViewById(R.id.postToolbar);
        setSupportActionBar(postToolbar);

        getSupportActionBar().setTitle(R.string.post_land);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myCommand = new MyCommand(getApplicationContext());
        userLocalStore = new UserLocalStore(getApplicationContext());

        loggedInUser = userLocalStore.getLoggedInUser();


        locationAddress = (TextView)findViewById(R.id.locationAddress);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);


        deposit = (EditText)findViewById(R.id.deposit);
        deposit.addTextChangedListener(new NumberTextWatcher(deposit, "#,###"));

        price = (EditText)findViewById(R.id.price);
        price.addTextChangedListener(new NumberTextWatcher(price, "#,###"));

        tagline = (EditText)findViewById(R.id.tagline);
        description = (EditText)findViewById(R.id.description);
        size = (EditText)findViewById(R.id.size);
        phoneNo = (EditText)findViewById(R.id.phoneNo);

        addLocation = (FloatingActionButton)findViewById(R.id.location);
        addPhotos = (FloatingActionButton)findViewById(R.id.photosFAB);
        continuePosting = (Button)findViewById(R.id.continuePosting);

        viewPager = (ViewPager) findViewById(R.id.uploadViewpager);
        viewPager.setVisibility(View.GONE);


        addLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                Intent intent;

                try {
                    intent = builder.build(PostLandActivity.this);
                    startActivityForResult(intent,PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        addPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoPicker.builder()
                        .setPhotoCount(1)
                        .setShowCamera(true)
                        .setShowGif(true)
                        .setPreviewEnabled(false)
                        .start(PostLandActivity.this, PhotoPicker.REQUEST_CODE);

            }
        });

        continuePosting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String priceTxt = "";
                String depositTxt = "";

                String locationTxt = tagline.getText().toString();
                String descriptionTxt = description.getText().toString();
                String sizeTxt = size.getText().toString();
                String phoneNoTxt = phoneNo.getText().toString();
                String emailTxt = userLocalStore.getLoggedInUser().getPersonEmail();
                if (!TextUtils.isEmpty(price.getText().toString()))
                {
                    String priceArr[] = price.getText().toString().split("\\.");
                    priceTxt = priceArr[1].replace(",","");
                }

                if (!TextUtils.isEmpty(deposit.getText().toString()))
                {
                    String depoArr[] = deposit.getText().toString().split("\\.");
                    depositTxt = depoArr[1].replace(",","");
                }



                PostLand postLand = new PostLand(selectedPhotos,latLong,locationTxt,descriptionTxt,
                        priceTxt,depositTxt,sizeTxt,phoneNoTxt,emailTxt);

                if (validateLand(postLand))
                {
                    try {
                        displayImageBitmap = com.softworld.crib.dependencies.ImageLoader.init()
                                .from(postLand.getSelectedPhotos().get(0))
                                .requestSize(720, 720).getBitmap();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    startUploadLand(postLand);
                    pDialog = new SweetAlertDialog(PostLandActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog.setTitleText("Processing...");
                    pDialog.setCancelable(false);
                    pDialog.show();
//

                }
            }
        });

        wDialog = new SweetAlertDialog(PostLandActivity.this, SweetAlertDialog.WARNING_TYPE);
        wDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        wDialog.setTitleText("Error :(");

        successDialog = new SweetAlertDialog(PostLandActivity.this, SweetAlertDialog.SUCCESS_TYPE);
        successDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        successDialog.setTitleText("Success! :)");
    }


    private boolean validateLand(PostLand postCrib) {
        boolean valid = true;

        if(postCrib.getSelectedPhotos().size() == 0)
        {
            Toast.makeText(PostLandActivity.this, R.string.select_photo, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }
        if(TextUtils.isEmpty(postCrib.getLatLong()))
        {
            Toast.makeText(PostLandActivity.this, R.string.set_location, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }

        if(TextUtils.isEmpty(postCrib.getLocationTxt()))
        {
            Toast.makeText(PostLandActivity.this, R.string.set_tagline, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }

        if(TextUtils.isEmpty(postCrib.getDescriptionTxt()))
        {
            Toast.makeText(PostLandActivity.this, R.string.set_description, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }

        if(TextUtils.isEmpty(postCrib.getPriceTxt()))
        {
            Toast.makeText(PostLandActivity.this, R.string.set_price, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }

        if(TextUtils.isEmpty(postCrib.getDepositTxt()))
        {
            Toast.makeText(PostLandActivity.this, R.string.set_depo, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }


        if(TextUtils.isEmpty(postCrib.getPhoneNoTxt()))
        {
            Toast.makeText(PostLandActivity.this, R.string.set_phone_no, Toast.LENGTH_LONG).show();
            valid = false;
            return valid;
        }
        return valid;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST)
        {
            if (resultCode == RESULT_OK)
            {
                Place place = PlacePicker.getPlace(this, data);
                locationAddress.setText(place.getAddress());
                Double latitude = place.getLatLng().latitude;
                Double longitude = place.getLatLng().longitude;
                latLong = latitude+", "+longitude;
                location = place.getAddress().toString();

            }
        }

        if (resultCode == RESULT_OK &&
                (requestCode == PhotoPicker.REQUEST_CODE || requestCode == PhotoPreview.REQUEST_CODE)) {

            List<String> photos = null;
            if (data != null) {
                photos = data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);
            }
            selectedPhotos.clear();

            if (photos != null) {

                selectedPhotos.addAll(photos);

                viewPager.setVisibility(View.VISIBLE);
                List<DataObject> imagesData = new ArrayList<DataObject>();

                for (int j = 0; j < selectedPhotos.size(); j++) {
                    imagesData.add(new DataObject(selectedPhotos.get(j)));
                }
                mCustomPagerAdapter = new CustomPageAdapter(PostLandActivity.this, imagesData);
                viewPager.setAdapter(mCustomPagerAdapter);
                Timer timer = new Timer();
                timer.schedule(new UpdateTimeTask(), 1000, 3000);
            }
        }

    }

    class UpdateTimeTask extends TimerTask {
        public void run() {
            viewPager.post(new Runnable() {
                public void run() {

                    if (viewPager.getCurrentItem() < mCustomPagerAdapter
                            .getCount() - 1) {
                        viewPager.setCurrentItem(
                                viewPager.getCurrentItem() + 1, true);
                    } else {
                        viewPager.setCurrentItem(0, true);

                    }
                }
            });
        }
    }

    private void showAddPhotosDialog(final String response) {
        addPhotosDialog = new Dialog(PostLandActivity.this);
        addPhotosDialog.setContentView(R.layout.add_photos_dialog);


        Button addNow, addLater;

        addLater = (Button) addPhotosDialog.findViewById(R.id.addLater);
        addLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPhotosDialog.dismiss();
                //showPayDialog();
                startActivity(new Intent(PostLandActivity.this, MainActivity.class));
                finish();
            }
        });



        addNow = (Button) addPhotosDialog.findViewById(R.id.addNow);
        addNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent mIntent = new Intent(PostLandActivity.this, MyProperty.class);
                mIntent.putExtra("myEmail", loggedInUser.getPersonEmail());
                startActivity(mIntent);
                finish();
            }
        });
        addPhotosDialog.show();
    }


    private void startUploadLand(final PostLand postCrib) {

        final String encodedDisplayImage = ImageBase64.encode(displayImageBitmap);

        StringRequest infoStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                pDialog.dismiss();
                successDialog.setContentText(getString(R.string.land_posted));
                successDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        successDialog.dismiss();
                        startActivity(new Intent(PostLandActivity.this, MainActivity.class));
                    }
                });
                successDialog.show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //error.printStackTrace();
                pDialog.dismiss();
                wDialog.setContentText(VolleyErrors.getVolleyErrorMessages(error, PostLandActivity.this));
                wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        wDialog.dismiss();
                        startActivity(new Intent(PostLandActivity.this, MainActivity.class));
                        finish();
                    }
                });
                wDialog.show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("latLong", postCrib.getLatLong());
                params.put("size", postCrib.getSizeTxt());
                params.put("deposit", postCrib.getDepositTxt());
                params.put("description", postCrib.getDescriptionTxt());
                params.put("email", postCrib.getEmailTxt());
                params.put("phoneNo", postCrib.getPhoneNoTxt());
                params.put("price", postCrib.getPriceTxt());
                params.put("location", postCrib.getLocationTxt());
                params.put("displayImage", encodedDisplayImage);

                return params;
            }
        };

        postCrib.getSelectedPhotos().clear();

        infoStringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(PostLandActivity.this);
        requestQueue.add(infoStringRequest);

    }



}
