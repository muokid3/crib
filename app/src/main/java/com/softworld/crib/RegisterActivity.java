package com.softworld.crib;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApiNotAvailableException;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.softworld.crib.dependencies.UserLocalStore;
import com.softworld.crib.models.User;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegisterActivity extends AppCompatActivity {
    private Toolbar signUpToolbar;
    private EditText confPasswordET,emailET, passwordET;
    private Button signUpBtn;
    private ProgressDialog pDialog;
    private FirebaseAuth fireBaseAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private SweetAlertDialog wDialog;
    private UserLocalStore userLocalStore;
    private String defaultUserPic;
    private Uri photoUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        signUpToolbar = (Toolbar)findViewById(R.id.signUpToolbar);
        setSupportActionBar(signUpToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sign Up");


        pDialog = new ProgressDialog(this);
        fireBaseAuth = FirebaseAuth.getInstance();

        defaultUserPic = "http://crib.co.ke/api/profilepic.png";

        wDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        wDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        wDialog.setTitle("Warning");

        if (fireBaseAuth.getCurrentUser() != null)
        {
            //start profile activity here
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }


        confPasswordET = (EditText)findViewById(R.id.confPasswordET);
        emailET = (EditText)findViewById(R.id.emailET);
        passwordET = (EditText)findViewById(R.id.passwordET);
        signUpBtn = (Button)findViewById(R.id.signUpBtn);

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

        userLocalStore = new UserLocalStore(this);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    String name = user.getDisplayName();
                    String email = user.getEmail();

                    if (user.getPhotoUrl() != null && !user.getPhotoUrl().equals(Uri.EMPTY)) {
                        //photo url is available
                        photoUrl = user.getPhotoUrl();
                    } else {
                        //photourl is null or empty
                        photoUrl = Uri.parse(defaultUserPic);

                    }

                    User loggedInUser = new User(name, email, photoUrl);
                    userLocalStore.storeUserData(loggedInUser);

                    startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                    finish();
                } else {
                    // User is signed out
                }
            }
        };

    }

    private void registerUser() {
        String email = emailET.getText().toString().trim();
        String password = passwordET.getText().toString().trim();
        String confPassword = confPasswordET.getText().toString().trim();

        if (TextUtils.isEmpty(email))
        {
            Toast.makeText(this, "Please provide email", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password))
        {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(confPassword))
        {
            Toast.makeText(this, "Please confirm your password", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!TextUtils.equals(password, confPassword))
        {
            Toast.makeText(this, "Please ensure both passwords match", Toast.LENGTH_SHORT).show();
            return;
        }

        pDialog.setMessage("Signing you up...");
        pDialog.show();

        fireBaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (!task.isSuccessful())
                        {
                            try {
                                throw task.getException();
                            } catch(FirebaseAuthWeakPasswordException e) {
                                wDialog.setTitleText("Error");
                                wDialog.setContentText("Please use a strong password");
                                wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        wDialog.dismiss();
                                        startActivity(new Intent(RegisterActivity.this, RegisterActivity.class));
                                        finish();
                                    }
                                });
                                wDialog.show();
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                wDialog.setTitleText("Error");
                                wDialog.setContentText("Invalid username or password.");
                                wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        wDialog.dismiss();
                                        startActivity(new Intent(RegisterActivity.this, RegisterActivity.class));
                                        finish();
                                    }
                                });
                                wDialog.show();
                            } catch(FirebaseAuthUserCollisionException e) {
                                wDialog.setTitleText("Error");
                                wDialog.setContentText("This email is already registered. Please sign in to continue");
                                wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        wDialog.dismiss();
                                        startActivity(new Intent(RegisterActivity.this, SignInActivity.class));
                                        finish();
                                    }
                                });
                                wDialog.show();
                            } catch(FirebaseApiNotAvailableException e) {
                                wDialog.setTitleText("Error");
                                wDialog.setContentText("Service is currently unavailable, please try again later");
                                wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        wDialog.dismiss();
                                        startActivity(new Intent(RegisterActivity.this, RegisterActivity.class));
                                        finish();
                                    }
                                });
                                wDialog.show();

                            }
                            catch(FirebaseNetworkException e) {
                                wDialog.setTitleText("Error");
                                wDialog.setContentText("Please check your network and try again");
                                wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        wDialog.dismiss();
                                        startActivity(new Intent(RegisterActivity.this, RegisterActivity.class));
                                        finish();
                                    }
                                });
                                wDialog.show();

                            }
                            catch(FirebaseException e) {
                                wDialog.setTitleText("Error");
                                wDialog.setContentText("We are having a problem processing your request. Please try again later");
                                wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        wDialog.dismiss();
                                        startActivity(new Intent(RegisterActivity.this, RegisterActivity.class));
                                        finish();
                                    }
                                });
                                wDialog.show();

                            } catch (Exception e) {
                                wDialog.setTitleText("Error");
                                wDialog.setContentText("We are having a problem processing your request. Please try again later");
                                wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        wDialog.dismiss();
                                        startActivity(new Intent(RegisterActivity.this, RegisterActivity.class));
                                        finish();
                                    }
                                });
                                wDialog.show();
                            }
                        }
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        fireBaseAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            fireBaseAuth.removeAuthStateListener(mAuthListener);
        }
    }

}
