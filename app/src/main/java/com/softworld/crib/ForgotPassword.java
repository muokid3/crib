package com.softworld.crib;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ForgotPassword extends AppCompatActivity {

    private EditText registered_emailid;
    private TextView backToLoginBtn, forgot_button;
    private SweetAlertDialog pDialog, sDialog, wDialog;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        auth = FirebaseAuth.getInstance();


        registered_emailid = (EditText)findViewById(R.id.registered_emailid);
        backToLoginBtn = (TextView)findViewById(R.id.backToLoginBtn);
        forgot_button = (TextView)findViewById(R.id.forgot_button);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Processing...");

        wDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        wDialog.setTitleText(":(");
        wDialog.setContentText("An error occurred, please try again");
        wDialog.setConfirmText("Ok");
        wDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                startActivity(new Intent(ForgotPassword.this, SignInActivity.class));
                finish();
            }
        });

        sDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        sDialog.setTitleText(":)");
        sDialog.setContentText("A password reset email was sent to your email");
        sDialog.setConfirmText("Ok");
        sDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                startActivity(new Intent(ForgotPassword.this, SignInActivity.class));
                finish();
            }
        });
        
        backToLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ForgotPassword.this, SignInActivity.class));
                finish();
            }
        });

        forgot_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(registered_emailid.getText().toString()))
                {
                    registered_emailid.setError("Please provide a registered email");
                }
                else
                {
                    doReset(registered_emailid.getText().toString());
                }
            }
        });

    }

    private void doReset(String emailAddress) {
        pDialog.show();
        auth.sendPasswordResetEmail(emailAddress)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        pDialog.dismiss();
                        if (task.isSuccessful()) {
                            sDialog.show();
                        }else
                        {
                            wDialog.show();
                        }
                    }
                });
    }
}
